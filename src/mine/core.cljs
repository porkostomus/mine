(ns ^:figwheel-hooks mine.core
    (:require [reagent.core :as reagent :refer [atom]]))

(def board-width 9)
(def board-height 9)

(defn rand-positions []
  (shuffle
    (for [i (range board-width)
          j (range board-height)]
      [i j])))

(defn set-mines [] 
  (loop [squares [1 1 1 1]]
    (if (= (count squares) (* board-width board-height))
       squares
      (recur (conj squares 0)))))

(defn init-matrix []
  (into {}
    (map vector
      (rand-positions)
      (set-mines))))

(defonce app-state
  (atom
    {:matrix (init-matrix)
     :stepped []
     :game-status :in-progress
     :message "Tread lightly..."}))

(defn blank [i j]
  [:rect
   {:width 0.9
    :height 0.9
    :fill "grey"
    :x (+ 0.05 i)
    :y (+ 0.05 j)
    :on-click
    (fn blank-click [e]
      (when (= (:game-status @app-state) :in-progress)
        (swap! app-state assoc-in [:stepped]
          (conj (:stepped @app-state) [i j]))
        (if (= 1 (get (:matrix @app-state) [i j]))
            (do (swap! app-state assoc :game-status :dead)
            (swap! app-state assoc :message "Fuck. You blew up."))
)))}])

(defn rect-cell
  [x y]
  [:rect.cell
   {:x (+ 0.05 x) :width 0.9
    :y (+ 0.05 y) :height 0.9
    :fill "white"
    :stroke-width 0.025
    :on-click #(println "titi")
    :stroke "black"}])

(defn text-cell [x y]
  [:text
   {:x (+ 0.5 x) :width 1
    :y (+ 0.72 y) :height 1
    :text-anchor "middle"
    :on-click #(println "toto")
    :font-size 0.6}
   (str (get (:matrix @app-state) [x y]))])

(defn cross [i j]
  [:g {:stroke "darkred"
       :stroke-width 0.4
       :stroke-linecap "round"
       :transform
       (str "translate(" (+ 0.5 i) "," (+ 0.5 j) ") "
            "scale(0.3)")}
   [:line {:x1 -1 :y1 -1 :x2 1 :y2 1}]
   [:line {:x1 1 :y1 -1 :x2 -1 :y2 1}]])

(defn render-board []
  (into
    [:svg.board
     {:view-box (str "0 0 " board-width " " board-height)
      :shape-rendering "auto"
      :style {:max-height "500px"}}]
    (for [i (range board-width)
          j (range board-height)]
      [:g
       [rect-cell i j]
       (if (some #{[i j]} (:stepped @app-state))
         (if (= 1 (get (:matrix @app-state) [i j]))
           [cross i j]
           [text-cell i j])      
           [blank i j])])))

(defn mine []
  [:center
     [:h1 (:message @app-state)]
     [:div [render-board]]])

(reagent/render-component [mine]
                          (. js/document (getElementById "app")))

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
